Install
=======

There are several components to a running instance of BillowConf:

* API Server
* Front end client
* Janus or Jitsi WebRTC Server
* STUN/TURN Servers (optional)

For setting up a basic instance, Jitsi can be used for WebRTC. This simplifies
setup, but limits the functionality of the system, as it does not allow
restriction on who is talking, who has video, etc.

BillowConf uses Google public STUN servers by default, but these can be replaced
in the API server configuration. TURN servers are used as a fail-safe when the
STUN protocol is unable to traverse the NAT setup between two clients. There are
no TURN servers configured by default, and administrators will have to host
their own if they wish to use any.
