# BillowConf

BillowConf is an online platform for virtual conferences. It supports different
rooms that people can join and interact with. Presenters give talks and can
enagage with the audience in real time through text (IRC) and video.

[Outline](https://writefreely.debian.social/paddatrapper/remote-conference-software)

[Documentation](https://billowconf.gitlab.io/billowconf/)

## Install

```bash
sudo apt install libxml2-dev libxslt-dev libjs-janus libjs-webrtc-adapter
virtualenv -p python3 pyenv
pyenv/bin/pip install -r requirements.txt
pyenv/bin/python manage.py migrate
pyenv/bin/python manage.py createsuperuser
```

## Run

```bash
cd frontend
npm run serve
```

In another terminal, start the django server:

```bash
pyenv/bin/python manage.py runserver
```

This will start a development web server on http://127.0.0.1:8000/

## Debian Dependencies

Most of the node dependencies are already in Debian. It currently is missing
vuex and vuetify. The frontend modules need symlinking into
`./frontend/node_modules/` for `npm` to pick them up. This approach is currently
untested.

### Backend

 * python3-django
 * python3-djangorestframework
 * python3-djangorestframework-filter
 * python3-django-webpack-loader

### Frontend Production

 * fonts-materialdesignicons-webfont
 * libjs-node-router
 * node-axios
 * node-core-js
 * node-vue-hot-reload-api
 * node-vue
 * node-webrtc-adapter

### Frontend Development
 * node-babel-eslint
 * node-bootstrap-sass
 * node-vue-template-compiler
 * npm
