###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
from django.contrib.auth.models import User, Group
from .models import Conference, Room, Talk, ConferenceManagementPerson, ResetPasswordToken
from rest_framework import serializers

class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'id', 'name']

class UserSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='last_name')

    def create(self, validated_data):
        validated_data['username'] = validated_data['email']
        return User.objects.create_user(**validated_data)

    def update(self, instance, validated_data):
        instance.username = validated_data['email']
        instance.email = validated_data['email']
        instance.last_name = validated_data['last_name']
        instance.is_staff = validated_data['is_staff']
        instance.save()
        return instance;

    class Meta:
        model = User
        fields = ['url', 'id', 'username', 'name', 'email', 'is_staff']
        read_only_fields = ['username']

class ConferenceManagementPersonSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username', read_only=True)

    class Meta:
        model = ConferenceManagementPerson
        fields = ['url', 'id', 'user', 'username', 'conference', 'management_id']


class ConferenceSerializer(serializers.ModelSerializer):

    class Meta:
        model = Conference
        fields = ['url', 'id', 'name', 'irc_server', 'start_date', 'end_date']

class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        model = Room
        fields = ['url', 'id', 'name', 'conference', 'irc_channel', 'webrtc_channel']
        read_only_fields = ['webrtc_channel']

class TalkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Talk
        fields = ['url', 'id', 'name', 'description', 'start', 'duration', 'presenters', 'room', 'recorded_content']

class TokenSerializer(serializers.ModelSerializer):
    token = serializers.CharField(source='key')
    class Meta:
        model = ResetPasswordToken
        fields = ['token']

class PasswordTokenSerializer(serializers.Serializer):
    password = serializers.CharField(style={'input_type': 'password'})
    token = serializers.CharField()

class EmailSerializer(serializers.Serializer):
    email = serializers.EmailField()
