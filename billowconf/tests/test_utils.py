###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import datetime
import pathlib
import os
from django.test import RequestFactory, TestCase
from django.contrib.auth.models import User
from django.db.models.signals import pre_save
from billowconf.utils.pentabarf import update_schedule
from billowconf.models import Conference, Room, ConferenceManagementPerson, Talk, create_webrtc_room

def get_name(obj):
    return obj.name

def get_username(user):
    return user.last_name

def get_cmp_username(user):
    return user.user.last_name

class UpdateScheduleTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        pre_save.disconnect(create_webrtc_room, Room)
        cls.conference = Conference(name='Test Conference',
                                    start_date=datetime.date.today(),
                                    end_date=datetime.date.today() + datetime.timedelta(days=3))
        cls.conference.save()

    @classmethod
    def tearDownClass(cls):
        pre_save.connect(create_webrtc_room, Room)


    def test_create_rooms(self):
        penta_schedule = '<schedule><day><room name="1"></room><room name="2"></room><room name="Elsewhere™"></room></day></schedule>'
        update_schedule(self.conference, penta_schedule)
        rooms = Room.objects.filter(conference=self.conference)
        self.assertQuerysetEqual(rooms, ['1', '2'],
                                 transform=get_name,
                                 ordered=False)

    def test_update_rooms(self):
        penta_schedule = '<schedule><day><room name="1"></room><room name="2"></room><room name="Elsewhere™"></room></day></schedule>'
        update_schedule(self.conference, penta_schedule)
        new_schedule = '<schedule><day><room name="4"></room><room name="2"></room><room name="Elsewhere™"></room></day></schedule>'
        rooms = Room.objects.filter(conference=self.conference)
        update_schedule(self.conference, new_schedule)
        self.assertQuerysetEqual(rooms, ['4', '2'],
                                 transform=get_name,
                                 ordered=False)

    def test_create_users(self):
        penta_schedule = '<schedule><day><room name="1"><event id="1"><persons><person id="1">test1</person><person id="2">test2</person></persons></event></room><room name="2"></room><room name="Elsewhere™"></room></day></schedule>'
        update_schedule(self.conference, penta_schedule)
        users = User.objects.all()
        conf_people = ConferenceManagementPerson.objects.filter(conference=self.conference)
        self.assertQuerysetEqual(users, ['test1', 'test2'],
                                 transform=get_username,
                                 ordered=False)
        self.assertQuerysetEqual(conf_people, ['test1', 'test2'],
                                 transform=get_cmp_username,
                                 ordered=False)

    def test_update_users(self):
        penta_schedule = '<schedule><day><room name="1"><event id="1"><persons><person id="1">test1</person><person id="2">test2</person></persons></event></room><room name="2"></room><room name="Elsewhere™"></room></day></schedule>'
        update_schedule(self.conference, penta_schedule)
        new_schedule = '<schedule><day><room name="1"><event id="1"><persons><person id="1">test1</person><person id="2">test3</person></persons></event></room><room name="2"></room><room name="Elsewhere™"></room></day></schedule>'
        update_schedule(self.conference, new_schedule)
        conf_people = ConferenceManagementPerson.objects.filter(conference=self.conference)
        self.assertQuerysetEqual(conf_people, ['test1', 'test3'],
                                 transform=get_cmp_username,
                                 ordered=False)

    def test_full_import(self):
        penta_schedule = ''
        filepath = os.path.join(pathlib.Path(__file__).parent.absolute(),
                                'data', 'pentabarf.xml')
        with open(filepath, 'rb') as fb:
            penta_schedule = fb.read()
        update_schedule(self.conference, penta_schedule)
        talks = Talk.objects.filter(room__conference=self.conference, name='Salsa CI - Debian Pipeline for Developers')
        self.assertEqual(1, len(talks))
