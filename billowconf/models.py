###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import random
import os
import binascii
from django.conf import settings
from django.db import models
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

class Conference(models.Model):
    name = models.CharField(max_length=255)
    irc_server = models.CharField(max_length=255, default='')
    start_date = models.DateField()
    end_date = models.DateField()

    def __str__(self):
        return self.name

class Room(models.Model):
    name = models.CharField(max_length=255)
    conference = models.ForeignKey(Conference, on_delete=models.CASCADE)
    irc_channel = models.CharField(max_length=200, default='')
    webrtc_channel = models.IntegerField(default=-1)

    @property
    def stream(self):
        return self.pk

    def __str__(self):
        return self.name

class Talk(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    start = models.DateTimeField()
    duration = models.DurationField()
    presenters = models.ManyToManyField('ConferenceManagementPerson')
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    recorded_content = models.FileField(upload_to='talks/', blank=True, null=True)

    def __str__(self):
        return f"{self.name} ({self.presenters})"

class ConferenceManagementPerson(models.Model):
    management_id = models.IntegerField()
    conference = models.ForeignKey(Conference, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.user}'

class ResetPasswordToken(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE,
                             related_name='password_reset_tokens')
    created_at = models.DateTimeField(auto_now_add=True)
    key = models.CharField(max_length=64, unique=True)

    @staticmethod
    def generate_key():
        length = random.randint(10, 50)
        return binascii.hexlify(os.urandom(50)).decode()[0:length]

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super().save(*args, **kwargs)

@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)

@receiver(pre_save, sender=Room)
def create_webrtc_room(sender, instance=None, **kwargs):
    # Required here to prevent circular dependency
    from .utils import create_webrtc_room
    if instance.webrtc_channel == -1:
        instance.webrtc_channel = create_webrtc_room(instance.name)

