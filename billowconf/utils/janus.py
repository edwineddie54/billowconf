###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import random
import requests
import string
from django.conf import settings

IGNORED_ROOMS = ['Elsewhere™','Somewhere™']
IGNORED_ROOMS = [x.lower() for x in IGNORED_ROOMS]

def transaction():
    return ''.join(random.choices(string.ascii_lowercase + string.digits, k=7))

def create_janus_room(room_name):
    janus_url = f'https://{settings.JANUS["SERVER"]}{settings.JANUS["ROOT"]}'
    session_response = requests.post(janus_url, json={
        'janus': 'create',
        'transaction': transaction(),
    })
    session_response.raise_for_status()
    session_id = session_response.json()['data']['id']
    session_url = f'{janus_url}/{session_id}'
    attach_response = requests.post(session_url, json={
        'janus': 'attach',
        'plugin': 'janus.plugin.videoroom',
        'transaction': transaction(),
    })
    attach_response.raise_for_status()
    plugin_id = attach_response.json()['data']['id']
    plugin_url = f'{session_url}/{plugin_id}'
    create_response = requests.post(plugin_url, json={
        'janus': 'message',
        'transaction': transaction(),
        'body': {
            'request': 'create',
            'description': room_name,
            'is_private': False,
        }
    })
    create_response.raise_for_status()
    room_id = create_response.json()['plugindata']['data']['room']
    return str(room_id)
