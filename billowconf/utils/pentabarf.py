###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import datetime
import random
import string
from lxml import etree
from django.contrib.auth.models import User
from billowconf.models import Room, Talk, ConferenceManagementPerson

IGNORED_ROOMS = ['Elsewhere™','Somewhere™']
IGNORED_ROOMS = [x.lower() for x in IGNORED_ROOMS]

def update_schedule(conference, penta_schedule):
    root = etree.fromstring(penta_schedule)
    days = [x for x in root if x.tag == 'day']
    room_elements = [room for day in days for room in day if room.get('name').lower() not in IGNORED_ROOMS]
    room_names = {room.get('name') for room in room_elements}

    rooms = []
    for room_name in room_names:
        room, created = Room.objects.get_or_create(name=room_name, conference=conference)
        rooms.append(room.id)

    event_elements = [event
                      for room in room_elements
                      for event in room
                      if event.tag == 'event']
    persons_elements = [child
                        for event in event_elements
                        for child in event
                        if child.tag == 'persons']

    presenters = {person for persons in persons_elements for person in persons}
    conf_people = []
    for presenter in presenters:
        username = ''.join(random.choices(string.ascii_lowercase, k=5))
        while User.objects.filter(username=f'{username}@example.com').exists():
            username = ''.join(random.choices(string.ascii_lowercase, k=5))
        users = User.objects.filter(last_name=presenter.text)
        if not users.exists():
            user = User.objects.create_user(username=f'{username}@example.com',
                                            last_name=presenter.text,
                                            email=f'{username}@example.com')
        else:
            user = users.get()
        conf_person, created = ConferenceManagementPerson.objects.get_or_create(user=user,
                                                                       conference=conference,
                                                                       management_id=presenter.get('id'))
        conf_people.append(conf_person.id)

    talks = []
    for event_element in event_elements:
        try:
            name = next(event_element.iter('title')).text
            start = next(event_element.iter('date')).text
            duration = next(event_element.iter('duration')).text
            description = next(event_element.iter('description')).text
            persons_element = next(event_element.iter('persons'))
            room_name = next(event_element.iter('room')).text

            start = datetime.datetime.fromisoformat(start)
            hours, minutes = duration.split(':')
            duration = datetime.timedelta(hours=int(hours), minutes=int(minutes))
            room = Room.objects.filter(conference=conference, name=room_name)[0]
            presenter_ids = [person.get('id') for person in persons_element]
            presenters = ConferenceManagementPerson.objects.filter(conference=conference,
                                                                   management_id__in=presenter_ids)
            talk, created = Talk.objects.get_or_create(name=name,
                                                       description=description,
                                                       start=start,
                                                       duration=duration,
                                                       room=room)
            talk.presenters.clear()

            for presenter in presenters:
                talk.presenters.add(presenter)

            talk.save()

            talks.append(talk.id)
            if not created:
                talk.name = name
                talk.description = description
                talk.start = start
                talk.duration = duration
                talk.room = room
                talk.save
        except StopIteration:
            # Empty event
            continue

    Room.objects.filter(conference=conference).exclude(id__in=rooms).delete()
    ConferenceManagementPerson.objects.filter(conference=conference).exclude(user_id__in=conf_people).delete()
    Talk.objects.filter(room__conference=conference).exclude(id__in=talks).delete()
