###########################################################################
# BillowConf is Copyright (C) 2020 Kyle Robbertze <kyle@debian.org>
#
# BillowConf is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3 as
# published by the Free Software Foundation.
#
# BillowConf is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with BillowConf. If not, see <http://www.gnu.org/licenses/>.
###########################################################################
import datetime
from django.core.mail import send_mail
from django.conf import settings
from django.contrib.auth.models import User, Group
from django.contrib.auth.password_validation import validate_password, get_password_validators
from django.template.loader import render_to_string
from django.shortcuts import render, get_object_or_404
from django.utils import timezone
from django.urls import reverse
from rest_framework import viewsets, permissions, status, views
from rest_framework.decorators import action, api_view, authentication_classes
from rest_framework.response import Response
from rest_framework.mixins import RetrieveModelMixin
from .serializers import UserSerializer, GroupSerializer, RoomSerializer, TalkSerializer, ConferenceSerializer, ConferenceManagementPersonSerializer, TokenSerializer, PasswordTokenSerializer, EmailSerializer
from .models import Conference, Room, Talk, ConferenceManagementPerson, ResetPasswordToken
from .permissions import IsAdminOrIsSelf, IsAdminOrReadOnly, IsAdminOrAuthenticatedReadOnly
from .utils.pentabarf import update_schedule

def index(request):
    webrtc_component = ''
    if hasattr(settings, 'JANUS'):
        webrtc_component = 'JANUS'
    elif hasattr(settings, 'JITSI'):
        webrtc_component = 'JITSI'

    context = {'webrtc_component': webrtc_component}
    return render(request, 'billowconf/index.html', context=context)

class GroupViewSet(viewsets.ModelViewSet):
    queryset = Group.objects.all().order_by('name')
    serializer_class = GroupSerializer
    permission_classes = [IsAdminOrAuthenticatedReadOnly]

class ConferenceManagementPersonViewSet(viewsets.ModelViewSet):
    queryset = ConferenceManagementPerson.objects.all().order_by('user')
    serializer_class = ConferenceManagementPersonSerializer
    permission_classes = [permissions.IsAdminUser]

class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('last_name')
    serializer_class = UserSerializer
    permission_classes = [IsAdminOrIsSelf]

    @action(detail=False)
    def current(self, request):
        user = request.user
        if user and user.is_authenticated:
            serializer = UserSerializer(user, context={'request': request})
            return Response(serializer.data)
        return Response('Not logged in', status=status.HTTP_401_UNAUTHORIZED)

class RoomViewSet(viewsets.ModelViewSet):
    queryset = Room.objects.all().order_by('name')
    serializer_class = RoomSerializer
    permission_classes = [IsAdminOrReadOnly]
    filterset_fields = ['name', 'conference']

class TalkViewSet(viewsets.ModelViewSet):
    queryset = Talk.objects.all().order_by('start')
    serializer_class = TalkSerializer
    permission_classes = [IsAdminOrReadOnly]
    filterset_fields = ['name', 'room']

class ConferenceViewSet(viewsets.ModelViewSet):
    queryset = Conference.objects.all().order_by('name')
    serializer_class = ConferenceSerializer
    permission_classes = [IsAdminOrReadOnly]

    @action(detail=False)
    def current(self, request):
        conferences = Conference.objects.filter(end_date__gte=datetime.date.today()).order_by('start_date')
        conference = conferences.first()
        if conference:
            serializer = ConferenceSerializer(conference, context={'request': request})
            return Response(serializer.data)
        return Response('No current conference found. Please create one',
                        status=status.HTTP_404_NOT_FOUND)

    @action(detail=True, methods=['get', 'post'])
    def schedule(self, request, pk=None):
        conference = get_object_or_404(Conference, pk=pk)
        schedule_file = request.data['file']
        update_schedule(conference, schedule_file.read())
        serializer = ConferenceSerializer(conference, context={'request': request})
        return Response(serializer.data)

    @action(detail=False)
    def webrtc(self, request):
        if hasattr(settings, 'JANUS'):
            return Response({
                'janus_url': settings.JANUS['SERVER'],
                'janus_root': settings.JANUS['ROOT'],
                'janus_ws_root': settings.JANUS['WEBSOCKET_ROOT'],
                'ice_servers': settings.JANUS['ICE_SERVERS'],
            })
        else:
            return Response({
                'jitsi_url': settings.JITSI['SERVER'],
            })

@api_view(['POST'])
def validate_password_reset_token(request, *args, **kwargs):
    serializer = TokenSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    token = serializer.validated_data['key']

    try:
        reset_password_token = ResetPasswordToken.objects.get(key=token)
        expiry_date = reset_password_token.created_at + datetime.timedelta(hours=24)

        if timezone.now() > expiry_date:
            reset_password_token.delete()
            return Response({'status': 'expired'}, status=status.HTTP_404_NOT_FOUND)
        return Response({'status': 'OK'})
    except ObjectDoesNotExist:
        return Response({'status': 'notfound'}, status=status.HTTP_404_NOT_FOUND)

@api_view(['POST'])
def reset_password_confirm(request, *args, **kwargs):
    serializer = PasswordTokenSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    password = serializer.validated_data['password']
    token = serializer.validated_data['token']

    try:
        reset_password_token = ResetPasswordToken.objects.get(key=token)
        expiry_date = reset_password_token.created_at + datetime.timedelta(hours=24)

        if timezone.now() > expiry_date:
            reset_password_token.delete()
            return Response({'status': 'expired'}, status=status.HTTP_404_NOT_FOUND)

        if reset_password_token.user.is_active:
            try:
                validate_password(password,
                                  user=reset_password_token.user,
                                  password_validators=get_password_validators(settings.AUTH_PASSWORD_VALIDATORS))
            except ValidationError as e:
                raise ValidationError({'password': e.message})

            reset_password_token.user.set_password(password)
            reset_password_token.user.save()

        ResetPasswordToken.objects.filter(user=reset_password_token.user).delete()
        return Response({'status': 'OK'})
    except ObjectDoesNotExist:
        return Response({'status': 'notfound'}, status=status.HTTP_404_NOT_FOUND)

@api_view(['POST'])
def reset_password_request(request, *args, **kwargs):
    serializer = EmailSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    email = serializer.validated_data['email']

    expired_limit = timezone.now() - datetime.timedelta(hours=24)
    ResetPasswordToken.objects.filter(created_at__lte=expired_limit).delete()
    users = User.objects.filter(username__iexact=email)

    user = None
    if users.exists():
        user = users.get()

    if user is None or not user.is_active:
        # Do not leak user details by announcing accounts that do not exist
        return Response({'status': 'OK'})

    token = None
    if user.password_reset_tokens.all().exists():
        token = user.password_reset_tokens.all().first()
    else:
        token = ResetPasswordToken.objects.create(user=user)

    host = request.META['HTTP_HOST']
    send_reset_email(token, f'{request.scheme}://{host}')
    return Response({'status': 'OK'})

def send_reset_email(token, url_base):
    subject = 'BillowConf password reset requested'
    sender = settings.EMAIL_FROM
    receiver = token.user.email
    context = {
        'name': token.user.get_full_name(),
        'reset_link': f'{url_base}/accounts/reset/{token.key}',
        'organization': settings.EMAIL_NAME,
    }
    message = render_to_string('billowconf/reset_password_email.txt', context=context)
    send_mail(subject, message, sender, [receiver], fail_silently=False)
